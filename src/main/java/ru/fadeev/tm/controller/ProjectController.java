package ru.fadeev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.enumerated.Status;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @GetMapping(value = "/project")
    public ModelAndView projectListGet() {
        @NotNull final List<ProjectDTO> projects = projectService.findAll().stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
        @NotNull final ModelAndView model = new ModelAndView("project_list");
        model.addObject("projects", projects);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/project/create")
    public ModelAndView projectCreateGet() {
        @NotNull final ModelAndView model = new ModelAndView("project_create");
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/project/create")
    public ModelAndView projectCreatePost(
            @RequestParam @NotNull final String name,
            @RequestParam @Nullable final String description,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date finishDate,
            @RequestParam @NotNull final Status status
    ) throws Exception {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setStartDate(startDate);
        project.setFinishDate(finishDate);
        project.setStatus(status);
        projectService.persist(project);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/project");
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/project/remove/{id}")
    public ModelAndView projectRemoveGet(@PathVariable @NotNull final String id) {
        projectService.remove(id);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/project");
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/project/view/{id}")
    public ModelAndView projectViewGet(@PathVariable @NotNull final String id) {
        @Nullable final Project project = projectService.findOne(id);
        @NotNull final ModelAndView model = new ModelAndView("project_view");
        if (project == null) {
            model.setStatus(HttpStatus.BAD_REQUEST);
            return model;
        }
        model.addObject("project", project);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/project/edit/{id}")
    public ModelAndView projectEditGet(@PathVariable @NotNull final String id) {
        @Nullable final ProjectDTO project = convertToDTO(projectService.findOne(id));
        @NotNull final ModelAndView model = new ModelAndView("project_edit");
        if (project == null) {
            model.setStatus(HttpStatus.BAD_REQUEST);
            return model;
        }
        model.addObject("project", project);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/project/edit/{id}")
    public ModelAndView projectEditPost(
            @PathVariable @Nullable String id,
            @RequestParam @Nullable final String name,
            @RequestParam @Nullable final String description,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date finishDate,
            @RequestParam @Nullable final Status status
    ) {
        @Nullable final Project project = projectService.findOne(id);
        @NotNull ModelAndView model = new ModelAndView("redirect:/project");
        if (project == null) {
            model.setStatus(HttpStatus.BAD_REQUEST);
            return model;
        }
        if (name != null && !name.isEmpty()) project.setName(name);
        if (description != null && !description.isEmpty()) project.setDescription(description);
        project.setStartDate(startDate);
        project.setFinishDate(finishDate);
        if (status != null) project.setStatus(status);
        projectService.merge(project);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @Nullable
    private ProjectDTO convertToDTO(@Nullable final Project project) {
        if (project == null) return null;
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setStartDate(project.getStartDate());
        projectDTO.setFinishDate(project.getFinishDate());
        if (project.getUser() != null) projectDTO.setUserId(project.getUser().getId());
        projectDTO.setStatus(project.getStatus());
        projectDTO.setCreationTime(project.getCreationTime());
        return projectDTO;
    }

}