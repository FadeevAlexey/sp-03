package ru.fadeev.tm.enumerated;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.exception.IllegalStatusException;

@RequiredArgsConstructor
public enum Status {

    PLANNED("Planned"),
    IN_PROGRESS("In progress"),
    DONE("Done");

    @Getter
    @NotNull
    private final String displayName;

    @NotNull
    public static Status getAllowableStatus(@NotNull String status) {
        switch (status.toLowerCase()) {
            case "planned":
                return Status.PLANNED;
            case "in progress":
                return Status.IN_PROGRESS;
            case "done":
                return Status.DONE;
            default:
                throw new IllegalStatusException("wrong status");
        }
    }

}