package ru.fadeev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.enumerated.Status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @GetMapping(value = "/task")
    public ModelAndView taskListGet() {
        @NotNull final List<TaskDTO> tasks = taskService.findAll().stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());

        @NotNull final ModelAndView model = new ModelAndView("task_list");
        model.addObject("tasks", tasks);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/task/create")
    public ModelAndView taskCreateGet() {
        @NotNull final List<String> projects = new ArrayList<>();
        projectService.findAll().forEach(project -> projects.add(project.getName()));
        @NotNull final ModelAndView model = new ModelAndView("task_create");
        model.addObject("projects", projects);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/task/create")
    public ModelAndView taskCreatePost(
            @RequestParam @NotNull final String project,
            @RequestParam @NotNull final String name,
            @RequestParam @Nullable final String description,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date finishDate,
            @RequestParam @NotNull final Status status
    ) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setStartDate(startDate);
        task.setFinishDate(finishDate);
        task.setStatus(status);
        @Nullable final String projectId = projectService.findIdByName(project);
        task.setProject(projectService.findOne(projectId));
        taskService.persist(task);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/task");
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/task/remove/{id}")
    public ModelAndView TaskRemoveGet(@PathVariable @NotNull final String id) {
        taskService.remove(id);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/task");
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/task/view/{id}")
    public ModelAndView taskViewGet(@PathVariable @NotNull final String id) {
        @Nullable final Task task = taskService.findOne(id);
        @NotNull final ModelAndView model = new ModelAndView("task_view");
        if (task == null) {
            model.setStatus(HttpStatus.BAD_REQUEST);
            return model;
        }
        model.addObject("task", convertToDTO(task));
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/task/edit/{id}")
    public ModelAndView taskEditGet(@PathVariable @NotNull final String id) {
        @NotNull final List<String> projects = new ArrayList<>();
        projectService.findAll().forEach(project -> projects.add(project.getName()));
        @Nullable final TaskDTO task = convertToDTO(taskService.findOne(id));
        @NotNull final ModelAndView model = new ModelAndView("task_edit");
        if (task == null) {
            model.setStatus(HttpStatus.BAD_REQUEST);
            return model;
        }
        model.addObject("task", task);
        model.addObject("projects",projects);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/task/edit/{id}")
    public ModelAndView taskEditPost(
            @PathVariable @Nullable String id,
            @RequestParam @NotNull final String project,
            @RequestParam @Nullable final String name,
            @RequestParam @Nullable final String description,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date finishDate,
            @RequestParam @Nullable final Status status
    ) {
        @Nullable final Task task = taskService.findOne(id);
        @NotNull ModelAndView model = new ModelAndView("redirect:/task");
        if (task == null) {
            model.setStatus(HttpStatus.BAD_REQUEST);
            return model;
        }
        @Nullable final String projectId = projectService.findIdByName(project);
        task.setProject(projectService.findOne(projectId));
        if (name != null && !name.isEmpty()) task.setName(name);
        if (description != null && !description.isEmpty()) task.setDescription(description);
        task.setStartDate(startDate);
        task.setFinishDate(finishDate);
        if (status != null) task.setStatus(status);
        taskService.merge(task);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @Nullable
    private TaskDTO convertToDTO(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        if (task.getProject() != null) taskDTO.setProjectId(task.getProject().getId());
        taskDTO.setStartDate(task.getStartDate());
        taskDTO.setFinishDate(task.getFinishDate());
        if (task.getUser() != null) taskDTO.setUserId(task.getUser().getId());
        taskDTO.setStatus(task.getStatus());
        taskDTO.setCreationTime(task.getCreationTime());
        taskDTO.setProjectName(task.getProject().getName());
        return taskDTO;
    }

}